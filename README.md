﻿A new readme
=====
- Punkt Eins
- Punkt Zwei
- Punkt Drei
- Punkt Vier
- Punkt Fünf
- Punkt Sechs
- Punkt Sieben
- Punkt Acht
- Punkt Neun
- Punkt Rebase 123

Merge-Konflikt
===
- Konflikt-Datei (lokal) stagen und comitten
- Remote-Konflikt-Datei pullen
- Konflikt lokal lösen
- Datei erneut stagen
- Datei erneut committen
- Datei pushen


git rebase --interactive ziel-commit
===
- Ausgangs-Commit auschecken
- In Console: git rebase --interactive ziel-commit  
ziel-commit kann ein Zweig oder ein Commit (z.B. 7a574a1) sein  
ziel-commit kann der Ausgangs-Commit gefolgt von einer Tilde und der Anzahl einzubindender Commits sein (z.B. git rebase --interactive 7a574a1~3)
- pick für direkt auf Ziel-Commit folgenden Commit
- squash für alle folgenden Commits
- (vim => i=insertmode, esc=mode verlassen, :x = speichern)
- nach dem Speichern eventuell Commit-Message ändern (vim)
- Speichern
- rebase interacitve wird ausgeführt

Weitere Informationen: https://help.github.com/articles/interactive-rebase
